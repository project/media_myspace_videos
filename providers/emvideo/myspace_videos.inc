<?php
/*
 * @file myspace_videos.inc
 * Provides myspace_videos integration for emvideo module
 * @copyright Copyright(c) 2010 Lee Rowlands
 * @license GPL v3 http://www.fsf.org/licensing/licenses/gpl.html
 * @author Lee Rowlands leerowlands at rowlands-bcs dot com
 * 
 */

/**
 *  This is the main URL for your provider.
 */
define('EMVIDEO_MYSPACE_VIDEOS_MAIN_URL', 'http://vids.myspace.com/');

/**
 *  This defines the version of the content data array that we serialize
 *  in emvideo_myspace_videos_data(). If we change the expected keys of that array,
 *  we must increment this value, which will allow older content to be updated
 *  to the new version automatically.
 */
define('EMVIDEO_MYSPACE_VIDEOS_DATA_VERSION', 1);

/**
 * hook emvideo_PROVIDER_info
 * This returns information relevant to a specific 3rd party video provider.
 *
 * @return
 *   A keyed array of strings requested by various admin and other forms.
 *    'provider' => The machine name of the provider. This must be the same as
 *      the base name of this filename, before the .inc extension.
 *    'name' => The translated name of the provider.
 *    'url' => The url to the main page for the provider.
 *    'settings_description' => A description of the provider that will be
 *      posted in the admin settings form.
 *    'supported_features' => An array of rows describing the state of certain
 *      supported features by the provider. These will be rendered in a table,
 *      with the columns being 'Feature', 'Supported', 'Notes'. In general,
 *      the 'Feature' column will give the name of the feature, 'Supported'
 *      will be Yes or No, and 'Notes' will give an optional description or
 *      caveats to the feature.
 */
function emvideo_myspace_videos_info() {
  $features = array(
    array(t('RSS Attachment'), t('Yes'), ''),
    array(t('Thumbnails'), t('Yes'), t('')),
    array(t('Full screen mode'), t('Yes'), t('You may customize the player to enable or disable full screen playback. Full screen mode is enabled by default.')),
  );
  return array(
    'provider' => 'myspace_videos',
    'name' => t('MySpace Videos'),
    'url' => EMVIDEO_MYSPACE_VIDEOS_MAIN_URL,
    'settings_description' => t('These settings specifically affect videos displayed from !myspace_videos.', array('!myspace_videos' => l(t('GodTube.com'), EMVIDEO_MYSPACE_VIDEOS_MAIN_URL))),
    'supported_features' => $features,
  );
}

/**
 *  hook emvideo_PROVIDER_settings
 *  This should return a subform to be added to the emvideo_settings() admin
 *  settings page.
 *
 *  Note that a form field set will already be provided at $form['myspace_videos'],
 *  so if you want specific provider settings within that field set, you should
 *  add the elements to that form array element.
 */
function emvideo_myspace_videos_settings() {
  // We'll add a field set of player options here. You may add other options
  // to this element, or remove the field set entirely if there are no
  // user-configurable options allowed by the myspace_videos provider.
  $form['myspace_videos']['player_options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Embedded video player options'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  // This is an option to set the video to full screen. You should remove this
  // option if it is not provided by the myspace_videos provider.
  $form['myspace_videos']['player_options']['emvideo_myspace_videos_full_screen'] = array(
    '#type' => 'checkbox',
    '#title' => t('Allow fullscreen'),
    '#default_value' => variable_get('emvideo_myspace_videos_full_screen', 1),
    '#description' => t('Allow users to view video using the entire computer screen.'),
  );

  return $form;
}

/**
 *  hook emvideo_PROVIDER_extract
 *
 *  This is called to extract the video code from a pasted URL or embed code.
 *
 *  We'll be passed a URL or the embed code from a video when an editor pastes
 *  that in the field's textfield. We'll need to either pass back an array of
 *  regex expressions to match, or do the matching ourselves and return the
 *  resulting video code.
 *
 *  @param $parse
 *    An optional string with the pasted URL or embed code.
 *  @return
 *    Either an array of regex expressions to be tested, or a string with the
 *    video code to be used. If the hook tests the code itself, it should
 *    return either the string of the video code (if matched), or an empty
 *    array. Otherwise, the calling function will handle testing the embed code
 *    against each regex string in the returned array.
 */
function emvideo_myspace_videos_extract($parse = '') {
  // Here we assume that a URL will be passed in the form of
  // http://www.myspace_videos.com/video/text-video-title
  // or embed code in the form of <object value="http://www.myspace_videos.com/embed...".

  // We'll simply return an array of regular expressions for Embedded Media
  // Field to handle for us.
  /*
   our embed scripts are
   <font face="Verdana" size="1" color="#999999"><br/><a href="http://www.myspace.com/video/vid/31755525" style="font: Verdana">Three-Headed Frog</a><br/><object width="425px" height="360px" ><param name="allowScriptAccess" value="always"/><param name="allowFullScreen" value="true"/><param name="wmode" value="transparent"/><param name="movie" value="http://mediaservices.myspace.com/services/media/embed.aspx/m=31755525,t=1,mt=video"/><embed src="http://mediaservices.myspace.com/services/media/embed.aspx/m=31755525,t=1,mt=video" width="425" height="360" allowFullScreen="true" type="application/x-shockwave-flash" wmode="transparent" allowScriptAccess="always"></embed></object><br/><a href="http://www.myspace.com/362847633" style="font: Verdana">Diagonal View</a> | <a href="http://vids.myspace.com" style="font: Verdana">Myspace Video</a></font><script type="text/javascript" src="http://www.myspace_videos.com/embed/source/7g7wy7nx/400/255/true.js"></script>
   or our urls are
   http://www.myspace.com/video/vid/31755525
   */
  return array(
    // In this expression, we're looking first for text matching the expression
    // between the @ signs. The 'i' at the end means we don't care about the
    // case. Thus, if someone enters http://www.GodTube.com, it will still
    // match. We escape periods as \., as otherwise they match any character.
    // The text in parentheses () will be returned as the provider video code,
    // if there's a match for the entire expression. In this particular case,
    // ([^?]+) means to match one more more characters (+) that are not a
    // question mark ([^\?]), which would denote a query in the URL.
    '@myspace\.com/video/vid/([^" \?]+)@i',
    //http://www.myspace.com/index.cfm?fuseaction=vids.individual&videoId=31755525
    '@myspace\.com/index\.cfm\?fuseaction=vids\.individual&videoId\=([^" \?]+)@i',
    
  );
}

/**
 *  hook emvideo_PROVIDER_data
 *
 *  Provides an array to be serialised and made available with $item elsewhere.
 *
 *  This data can be used to store any extraneous information available
 *  specifically to the myspace_videos provider.
 */
function emvideo_myspace_videos_data($field, $item) {
  // Initialize the data array.
  $data = array();

  // Create some version control. Thus if we make changes to the data array
  // down the road, we can respect older content. If allowed by Embedded Media
  // Field, any older content will automatically update this array as needed.
  // In any case, you should account for the version if you increment it.
  $data['emvideo_myspace_videos_version'] = EMVIDEO_MYSPACE_VIDEOS_DATA_VERSION;

  // We are using oEmbed to retrieve a standard set of data from the provider.
  // You should change the URL as specified by the provider.
  // If the myspace_videos provider does not support oEmbed, you must remove this
  // section entirely, or rewrite it to use their API.
  // See http://oembed.com/ for for information.
  $url = urlencode('http://www.myspace.com/video/vid/'. $item['value']);
  $api_url = 'http://api.embed.ly/1/oembed?url='. $url;
  $xml = emfield_request_xml('myspace_videos', $api_url,
                             array(),
                             TRUE,
                             FALSE,
                             $item['value'],
                             FALSE,
                             TRUE);
  // This stores a URL to the video's thumbnail.
  $data['thumbnail'] = $xml['thumbnail_url'];
  $data = array_merge($data, $xml);
  return $data;
}

/**
 *  hook emvideo_PROVIDER_rss
 *
 *  This attaches a file to an RSS feed.
 */
function emvideo_myspace_videos_rss($item, $teaser = NULL) {
  if ($item['value']) {
    $file['thumbnail']['filepath'] = $item['data']['thumbnail'];

    return $file;
  }
}

/**
 * hook emvideo_PROVIDER_embedded_link($video_code)
 * returns a link to view the video at the provider's site.
 *  @param $video_code
 *    The string containing the video to watch.
 *  @return
 *    A string containing the URL to view the video at the original provider's site.
 */
function emvideo_myspace_videos_embedded_link($video_code) {
  return 'http://www.myspace.com/video/vid/'. $video_code;
}

/**
 * The embedded flash displaying the myspace_videos video.
 */
function theme_emvideo_myspace_videos_flash($item, $width, $height, $autoplay) {
  $output = '';
  if ($item['embed']) {
    /*
     <object width="425px" height="360px" >
     <param name="allowScriptAccess" value="always"/>
     <param name="allowFullScreen" value="true"/>
     <param name="wmode" value="transparent"/>
     <param name="movie" value="http://mediaservices.myspace.com/services/media/embed.aspx/m=31755525,t=1,mt=video"/>
     <embed src="http://mediaservices.myspace.com/services/media/embed.aspx/m=31755525,t=1,mt=video"
     width="425" height="360" allowFullScreen="true"
     type="application/x-shockwave-flash" wmode="transparent" allowScriptAccess="always">
     </embed></object>*/
    $autoplay = $autoplay ? 'true' : 'false';
    $fullscreen = variable_get('emvideo_myspace_videos_full_screen', 1) ? 'true' : 'false';
    $output = '<object width="'. $width .'px" height="'. $height .'px" >
     <param name="allowScriptAccess" value="always"/>
     <param name="allowFullScreen" value="'. $fullscreen .'"/>
     <param name="wmode" value="transparent"/>
     <param name="movie" value="http://mediaservices.myspace.com/services/media/embed.aspx/m='. $item['value'] .',t=1,mt=video"/>
     <embed src="http://mediaservices.myspace.com/services/media/embed.aspx/m='. $item['value'] .',t=1,mt=video"
     width="'. $width .'" height="'. $height .'" allowFullScreen="'. $fullscreen .'"
     type="application/x-shockwave-flash" wmode="transparent" allowScriptAccess="always">
     </embed></object>';
  }
  return $output;
}

/**
 * hook emvideo_PROVIDER_thumbnail
 * Returns the external url for a thumbnail of a specific video.
 *  @param $field
 *    The field of the requesting node.
 *  @param $item
 *    The actual content of the field from the requesting node.
 *  @return
 *    A URL pointing to the thumbnail.
 */
function emvideo_myspace_videos_thumbnail($field, $item, $formatter, $node, $width, $height) {
  // In this demonstration, we previously retrieved a thumbnail using oEmbed
  // during the data hook.
  return $item['data']['thumbnail'];
}

/**
 *  hook emvideo_PROVIDER_video
 *  This actually displays the full/normal-sized video we want, usually on the
 *  default page view.
 *  @param $embed
 *    The video code for the video to embed.
 *  @param $width
 *    The width to display the video.
 *  @param $height
 *    The height to display the video.
 *  @param $field
 *    The field info from the requesting node.
 *  @param $item
 *    The actual content from the field.
 *  @return
 *    The html of the embedded video.
 */
function emvideo_myspace_videos_video($embed, $width, $height, $field, $item, $node, $autoplay) {
  $output = theme('emvideo_myspace_videos_flash', $item, $width, $height, $autoplay);
  return $output;
}

/**
 *  hook emvideo_PROVIDER_video
 *
 *  This actually displays the preview-sized video we want, commonly for the
 *  teaser.
 *  @param $embed
 *    The video code for the video to embed.
 *  @param $width
 *    The width to display the video.
 *  @param $height
 *    The height to display the video.
 *  @param $field
 *    The field info from the requesting node.
 *  @param $item
 *    The actual content from the field.
 *  @return
 *    The html of the embedded video.
 */
function emvideo_myspace_videos_preview($embed, $width, $height, $field, $item, $node, $autoplay) {
  $output = theme('emvideo_myspace_videos_flash', $item, $width, $height, $autoplay);
  return $output;
}

/**
 *  Implementation of hook_emfield_subtheme.
 *  This returns any theme functions defined by this provider.
 */
function emvideo_myspace_videos_emfield_subtheme() {
  $themes = array(
      'emvideo_myspace_videos_flash'  => array(
          'arguments' => array('item' => NULL, 'width' => NULL, 'height' => NULL, 'autoplay' => NULL),
          'file' => 'providers/myspace_videos.inc',
          // If you don't provide a 'path' value, then it will default to
          // the emvideo.module path. Obviously, replace 'emmyspace_videos' with
          // the actual name of your custom module.
          'path' => drupal_get_path('module', 'media_myspace_videos'),
      )
  );
  return $themes;
}
